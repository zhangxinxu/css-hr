# CSS hr分隔线

#### 介绍
使用纯CSS和HTML `<hr>`元素实现各式各样的分隔线效果

#### 说明

index.html就是demo页面，项目clone到本地后，可以直接点击访问。

或者直接点击这里访问：https://zhangxinxu.gitee.io/css-hr/

hr.css里面就是十几个下划线的样式合集，大家可以直接在项目中应用，也可以只拷贝需要的下划线样式代码。

另外，如果不想clone到本地，也可以直接访问文章介绍页面：<a href="https://www.zhangxinxu.com/wordpress/?p=9962">https://www.zhangxinxu.com/wordpress/?p=9962</a>

其中有源码和对应的效果。


#### 其他

欢迎补充其他一些实用的下划线样式。

感谢关注此项目！
